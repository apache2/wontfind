# PoC for sysctl infoleak

The "proc" sysctls `vm.objects` and `vm.swap_objects` reveal (or used to reveal) a number of data points for files that have been accessed since boot (the list `vm_object_list`): size (rounded to pages), path name (when applicable), inode/fsid, number of resident pages, etc. The struct for each entry is `kinfo_vmobject` found in `sys/sys/user.h`. The sysctls are implemented in `sys/vm/vm_object.c`.

This is a problem because callers of these sysctl can learn information about files that they normally wouldn't be able to see through the VFS (because they resided in a different jail, or a folder that the caller can't access).

Relevant follow-up commits:
- [https://git.hardenedbsd.org/hardenedbsd/HardenedBSD/-/commit/20177e60364cbf56df0b71b74a00e10e9646d087](HardenedBSD commit limiting the sysctls to the non-jail root user)
- [https://cgit.freebsd.org/src/commit/?id=38f5f2a4af5daeec7f13d39cad1ff4dc90da52d8](FreeBSD commit limiting file path information to non-jailed callers)

# `wontfind`

This utility works more or less like the `find` command, except instead
of traversing the filesystem, it uses the `vm.objects` sysctl node.

If you don't care for my artisanal command-line utility, you
can run `sysctl -x -B 9999999 vm.objects` and parse the output yourself.

This has several benefits:
- just two system calls to get a list of all files that have been opened on the system since last boot
- doesn't care about read nor search permissions
- **lists files on the host system,** not *just* the jail you happen to be logged into.
- and will thus let you get filenames, inodes, and approximate file size, for files you had no idea existed because they were locked away in directories that `find` couldn't access.

This command is for filtering files you *won't* find with `find`. :-)

### Example:

```
make

# See which small files root is hiding from you:
./wontfind -path /root -not -findable -size -1G
...

# Find a secret file:
root@hbsdev:~ # ls -l /root/.secret/
total 1
-rw-r-----  1 root wheel 0 Jan 12 06:55 my-secret-file
nobody@hbsdev:~ /tmp/wontfind -not -findable -path /root/'*secret*'
 /root/.secret/my-secret-file

# Find PHP session IDs:
./wontfind -path '*/sess_*' -size +0
...
```

### Supported flags:

```
-not        negate the following test
-findable   things `find` can find, access(F_OK) test
-inum       inode
-size       file size (-ish. rounded up to nearest page size).
-path       globbing across the whole path.
-readable   like find's -readable, access(R_OK)
```
