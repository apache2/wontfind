/*
 * apache2, 2024-01-12
 *
 * the sysctl vm.objects leads to
 * sys/vm/vm_object.c:vm_object_list_handler()
 *
 */
#include <err.h>
#include <stdio.h>
#include <sys/sysctl.h>
#include <stdlib.h>
#include <limits.h>
#include <unistd.h>
#include <sys/user.h>
#include <fnmatch.h>
#include <string.h>

typedef enum {
	DONTFIND_END      = 0,
	DONTFIND_NOT      = 1,
	DONTFIND_PATH     = 2,
	DONTFIND_SIZE     = 4,
	DONTFIND_INODE    = 8,
	DONTFIND_READABLE = 16,
	DONTFIND_CMP_LT   = 32,
	DONTFIND_CMP_EQ   = 64,
	DONTFIND_CMP_GT   = 128,
	DONTFIND_FINDABLE = 256,
} dontfind_filter_op;

typedef union {
	dontfind_filter_op op;
	char *str;
	size_t size;
} dontfind_filter;

static int
eval_int(dontfind_filter_op op, size_t a, size_t b)
{
	switch (op) {
	case DONTFIND_CMP_EQ: return (a == b);
	case DONTFIND_CMP_LT: return (a < b);
	case DONTFIND_CMP_GT: return (a > b);
	default:
		errx(5, "eval_int: need CMP");
	}
}

int
foreach(const char *const vmobjects, const size_t vmobjects_size, dontfind_filter*const filters)
{
	const char * p = vmobjects;
	const char *const stop = vmobjects + vmobjects_size;
	while(p < stop)
	{
		struct kinfo_vmobject *o;
		o = (typeof(o)) p;

		int show = 1;
		const dontfind_filter *f = filters;
		int negated = 0;
		while (show && DONTFIND_END != (*f).op) {
			switch ((*f++).op)
			{
			case DONTFIND_END: { errx(123,"BAD END\n"); break; }
			case DONTFIND_NOT: {
				negated ^= 1;
				break;
			}
			case DONTFIND_READABLE: {
				show &= o->kvo_path[0] && !access(o->kvo_path, R_OK);
				show ^= negated; negated = 0;
				break;
			}
			case DONTFIND_FINDABLE: {
				show &= o->kvo_path[0] && !access(o->kvo_path, F_OK);
				show ^= negated; negated = 0;
				break;
			}
			case DONTFIND_INODE: {
				dontfind_filter_op op = (*f++).op;
				show &= eval_int(op, o->kvo_vn_fileid, (*f++).size);
				show ^= negated; negated = 0;
				break;
			}
			case DONTFIND_SIZE: {
				dontfind_filter_op op = (*f++).op;
				show &= eval_int(op, o->kvo_size, (*f++).size);
				show ^= negated; negated = 0;
				break;
			}
			case DONTFIND_PATH: {
				if (o->kvo_path[0]) {
					show &= !fnmatch((*f++).str, o->kvo_path, FNM_CASEFOLD | FNM_LEADING_DIR);
				} else {
					show &= 0;
				}
				show ^= negated; negated = 0;
				break;
			}
			default: {
				errx(2, "unknown opcode");
			}
			}
		}
		if (!show) {
			goto next;
		}
		/*
		 * see struct kinfo_vmobject in sys/user.h
		 */
		/*
		  printf("(vm_object_t *)%p entry in &vm_object_list\n", (void*)o->kvo_me);
		  printf("type:%d size:%Lu inode:%Lu\n", o->kvo_type,
		  o->kvo_size, o->kvo_vn_fileid);
		*/

		if (o->kvo_path[0]) {
			printf(" %s\n", o->kvo_path);
		}
next:
		p += o->kvo_structsize;
	}
	return 0;
}

int
dontfind_vm_objects(char **oldp, size_t *oldlenp)
{
	int err = 0;
	err = sysctlbyname("vm.objects", NULL, oldlenp, NULL, 0);
	if (err) {
		return err;
	}
	*oldp = calloc(*oldlenp, 1);
	if (NULL == *oldp) {
		return (-1);
	}
	err = sysctlbyname("vm.objects", *oldp, oldlenp, NULL, 0);
	if (err) {
		free(*oldp);
	}
	return err;
}


static int
parse_int(dontfind_filter **old_push, char **argv, int *i)
{
	dontfind_filter *push =  *old_push;
	size_t skip_first = 1;
	switch (argv[*i][0])
	{
	case '-': {
		(*push++).op = DONTFIND_CMP_LT;
		break;
	}
	case '+': {
		(*push++).op = DONTFIND_CMP_GT;
		break;
	}
	default: {
		skip_first = 0;
		(*push++).op = DONTFIND_CMP_EQ;
		break;
	}
	}
	errno = 0;
	char *last = NULL;
	(*push).size = strtoll(argv[*i] + skip_first, &last, 10);
	if (errno) {
		return errno;
	}
	switch (last ? *last : 0) {
	case 'G': {(*push).size *= 1024*1024*1024; break; }
	case 'M': {(*push).size *= 1024*1024; break; }
	case 'K': {(*push).size *= 1024; break; }
	case 'c': { break; }
	case 0: break;
	default: {
		return 1;
	}
	}
	*old_push = ++push;
	return 0;
}

int
main(int argc, char**argv)
{
	int err = 0;
	size_t oldlenp = 0;
	char *oldp = NULL;
	err = dontfind_vm_objects(&(oldp), &oldlenp);
	if (err) {
		perror("sysctlbyname");
		exit(err);
	}

	dontfind_filter *filters = calloc(sizeof(dontfind_filter), argc+1);
	if (NULL == filters) {
		return 2;
	}
	dontfind_filter *push = filters;
	for (int i=1; i<argc; i++) {
		if (!strcmp("-path", argv[i])) {
			(*push++).op = DONTFIND_PATH;
			(*push++).str = argv[++i];
		}
		else if (!strcmp("-size", argv[i])) {
			(*push++).op = DONTFIND_SIZE;
			i++;
			if (parse_int(&push, argv, &i)) {
				errx(4, "-size: can't parse: %s\n", argv[i]);
			}
		}
		else if (!strcmp("-inum", argv[i])) {
			(*push++).op = DONTFIND_INODE;
			i++;
			if (parse_int(&push, argv, &i)) {
				errx(6, "-inum: can't parse: %s\n", argv[i]);
			}
		}
		else if (!strcmp("-readable", argv[i])) {
			(*push++).op = DONTFIND_READABLE;
		}
		else if (!strcmp("-not", argv[i])) {
			(*push++).op = DONTFIND_NOT;
		}
		else if (!strcmp("-findable", argv[i])) {
			(*push++).op = DONTFIND_FINDABLE;
		}
		else {
			errx(1, "unknown flag: %s\n", argv[i]);
		}
	}
	(*push++).op = DONTFIND_END;

	err = foreach(oldp, oldlenp, filters);

	return err;
}
